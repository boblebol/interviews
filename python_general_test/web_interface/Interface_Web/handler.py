import tornado.web
import test


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('main.html')


class SigninHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('signin.html')

    def post(self):
        if not self.get_argument("username", ""):
            self.render('error.html', error_msg="Please provide a username.")
        if not self.get_argument("password", ""):
            self.render('error.html', error_msg="Please provide a password.")

        format = test.username_format(self.get_argument("username", ""))
        if format == "Error":
            self.render('error.html', error_msg="Wrong username formatting")
        else:
            cred = test.signin(self.get_argument("username", ""), self.get_argument("password", ""))
            if cred == "Success":
                self.render('example.html')
            else:
                self.render('error.html', error_msg="Wrong credentials")


class ExampleHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('example.html')

    def post(self):
        operand1 = self.get_argument("operand1", "")
        operand2 = self.get_argument("operand2", "")
        operator = self.get_argument("operator", "")
        operator_conversion_dict = {'addition': '+',
                                    'soustraction': '-',
                                    'produit': '*',
                                    'division': '/'
                                    }
        print(operand1, operand2, operator)
        print(type(operand1), type(operand2), type(operator))
        try:
            result = test.run_example(operand1, operand2, operator)
            result_full_operation = "%s %s %s = %s" % (operand1, operator_conversion_dict[operator], operand2, result[0])
            print(result_full_operation)
            self.render('result.html', result=result_full_operation)
        except Exception as e:
            self.render('error.html', error_msg=repr(e))
