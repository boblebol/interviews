#! /usr/bin/env python
# coding: utf-8

import os
import sys
import handler
from tornado.options import define, options
import tornado.httpserver
import tornado.web

DEBUG = True
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
define('port', default=12000, help='run on the given port', type=int)  # Default port is set to 12000, but you can change it at your own convenience
define('debug', default=DEBUG, type=bool)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', handler.MainHandler),  # Main URL Path
            (r'/signin', handler.SigninHandler),  # URL Path to display the authentication form, provided as an example
            (r'/example', handler.ExampleHandler),  # URL Path provided as an example
        ]

        settings = {
            'template_path': os.path.join(PROJECT_PATH, 'view/templates'),
            'static_path': os.path.join(PROJECT_PATH, 'view/static'),
            'debug': True
        }
        tornado.web.Application.__init__(self, handlers, **settings)

    
if __name__ == '__main__':
    if not DEBUG:
        # Redirect to log file
        so = se = open('static/server.log', 'w', 0)
        sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

    tornado.options.parse_command_line()
    print ('Running server on port %s...' % options.port)

    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
