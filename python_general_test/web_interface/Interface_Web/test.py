# coding: utf-8

import re


def username_format(username):
    if not re.match(r'[A-Z][A-Za-z0-9]{7,}', username, flags=0):
        return "Error"
    else:
        return "Success"


def signin(username, password):
    if isinstance(username, str) and isinstance(password, str) and username == password [::-1]:
        return "Success"
    else:
        return "Error"


def run_example(operand1, operand2, operator):
    if not isinstance(operand1, int) or not isinstance(operand1, float):
        operand1 = float(operand1)
    if not isinstance(operand2, int) or not isinstance(operand2, float):
        operand2 = float(operand2)
    if operator == 'addition':
        return [operand1+operand2]
    if operator == 'soustraction':
        return [operand1 - operand2]
    if operator == 'produit':
        return [operand1 * operand2]
    if operator == 'division':
        return [operand1 / operand2]
    return ["not defined"]


