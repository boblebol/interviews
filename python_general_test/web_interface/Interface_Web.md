#Test Interface Web



## Consignes
Considérons le code source fourni.

Pour démarrer le projet, ouvrir un terminal dans le dossier courant du projet et exécuter la
commande suivante : python app.py

Puis, ouvrir un navigateur web et entrer le lien “localhost:12000”



Q1 : le lien « Sign in » propose d’entrer un « username / password » pour accéder à la page «
example.html ». En l’état, n’importe quel mots entrés dans ces 2 champs donnent accès à cette page.
Nous souhaitons sécuriser l’opération en appliquant la restriction suivante : « username » doit
obligatoirement commencer par une lettre majuscule et contenir plus de 8 caractères alpha-
numérique au total, sans accents ni caractères spéciaux. « password » correspond au username
inversé.



Q2 : La page « example.html » qui s’affiche propose d’entrer 2 champs « operand 1 » et « operand 2
» et de choisir un « operator ». A ce stade, seul l’addition est prise en compte. Compléter le code
source pour intégrer « soustraction », « produit » & « division » tout en faisant attention aux
exceptions qui peuvent être levées. Nous souhaitons également que la page affichant le résultat
rappelle l’opération qui a été faite. (exemple : « 3 + 7 = 10 »)

## Solutions 
### Question 1
Modification simple du fichier _Interface Web/handler.py_ :
```python
class SigninHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('signin.html')

    def post(self):
        if not self.get_argument("username", ""):
            self.render('error.html', error_msg="Please provide a username.")
        if not self.get_argument("password", ""):
            self.render('error.html', error_msg="Please provide a password.")

        format = test.username_format(self.get_argument("username", ""))
        if format == "Error":
            self.render('error.html', error_msg="Wrong username formatting")
        else:
            cred = test.signin(self.get_argument("username", ""), self.get_argument("password", ""))
            if cred == "Success":
                self.render('example.html')
            else:
                self.render('error.html', error_msg="Wrong credentials")
```

### Question 2
Modification simple du fichier _Interface Web/handler.py_  && _Interface Web/test.py_ pour gerer les différents opérateurs:

**handler.py:**
```python
class ExampleHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('example.html')

    def post(self):
        operand1 = self.get_argument("operand1", "")
        operand2 = self.get_argument("operand2", "")
        operator = self.get_argument("operator", "")
        operator_conversion_dict = {'addition': '+',
                                    'soustraction': '-',
                                    'produit': '*',
                                    'division': '/'
                                    }
        print(operand1, operand2, operator)
        print(type(operand1), type(operand2), type(operator))
        try:
            result = test.run_example(operand1, operand2, operator)
            result_full_operation = "%s %s %s = %s" % (operand1, operator_conversion_dict[operator], operand2, result[0])
            print(result_full_operation)
            self.render('result.html', result=result_full_operation)
        except Exception as e:
            self.render('error.html', error_msg=repr(e))
```

**test.py :**
```python
def run_example(operand1, operand2, operator):
    if not isinstance(operand1, int) or not isinstance(operand1, float):
        operand1 = float(operand1)
    if not isinstance(operand2, int) or not isinstance(operand2, float):
        operand2 = float(operand2)
    if operator == 'addition':
        return [operand1+operand2]
    if operator == 'soustraction':
        return [operand1 - operand2]
    if operator == 'produit':
        return [operand1 * operand2]
    if operator == 'division':
        return [operand1 / operand2]
    return ["not defined"]
```

et modification du fichier _Interface Web/view/templates/result.html_ pour modifier le rendu du resultat :

**result.html:**
```html
{% extends main.html %}

{% block content %}
<h3>Results</h3>

<div class="container">
    <p>
        {{result}}
    </p>
</div>
{% end %}
```
