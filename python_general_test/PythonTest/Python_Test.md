# Test Python
Pour chaque exercice, merci d’indiquer la liste des librairies/packages utilisés ainsi que la durée d’exécution de la fonction. (Ne pas oublier de mettre votre nom et prénom en entête)

## Exercices
A) Nous souhaitons analyser un fichier contenant 2 colonnes : [user_id, group_id]

(fichier "users_group.csv")

[ Excercice 1 ] : Sachant qu’un utilisateur peut appartenir à plusieurs groupes mais que son apparatenance à un
groupe ne doit être comptabilisée qu’une seule fois, nous voulons savoir combien d'utilisateurs (user_id) sont
présents dans le fichier 1 seule fois et combien sont présents au moins 2 fois

[ Excercice 2 ] : un utilisateur peut appartenir à plusieurs groupes. Aussi, nous voulons construire l'objet
contenant les données suivantes : où "VAL" est le nombre d'utilisateurs uniques communs aux 2 groupes

                      ____________________________________
                      | group1 | group2 | ...   | groupN |
              ____________________________________________
             | group1 |   5    |   2    | ...            |
              ____________________________________________
             | group2 |   2    |   8    | ...            |
              ____________________________________________
             | groupM | ...                         VAL |
              ____________________________________________

B) Nous souhaitons maintenant agréger les élements de deux fichiers à 2 colonnes chacun [user_id,
nombre_de_visites]

(fichiers "nb_visites1.csv" & "nb_visites2.csv")

[ Excercice 3 ] : Proposer une solution permettant de générer un seul objet avec les données agrégées par
"user_ids" à partir de N fichiers sources.

## Solutions
Librairies/packages utilisés : pandas, csv & timeit.

Pour cet exercice l'installation de pandans est necessaire.
```bash
pip install pandas
```
Les solutions sont dans le fichier _./python_test.py_, chaque fonction est commentée.
