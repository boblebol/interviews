#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Librairies/packages utilisés : pandas, csv & timeit

import csv
import timeit

import pandas


# Fonction annexe
def timeit_wrapper(func, *args, **kwargs):
    """
    Decorator to wrap function with arguments into a function without arguments in order to pass it into timeit.timeit
    :param func: function name you want to wrap
    :param args: List of unnamed argument to pass to the function
    :param kwargs: Dict of named argument to pass to the function
    :return: The return of the function
    """
    def wrapped():
        return func(*args, **kwargs)
    return wrapped


# Exercice 1
def count_uid_appear_one_or_more_in_csv(csv_file_path):
    """
    This function take a csv file path, in this file the uid in first column,
    and it return the tuple (number of uid present one time, number of uid present more than one time) in the file.
    :param csv_file_path:
    :type csv_file_path: String
    :return: the list number of uid present one time, number of uid present more than one time in the file.
    """
    uid_present_one_time_s = set()
    uid_present_two_time_or_more_s = set()
    uid_s = set()
    with open(csv_file_path, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t')
        for uid, group in spamreader:
            uid_s.add(uid)
            if uid not in uid_present_two_time_or_more_s:
                if uid not in uid_present_one_time_s:
                    uid_present_one_time_s.add(uid)
                else:
                    uid_present_two_time_or_more_s.add(uid)
                    uid_present_one_time_s.remove(uid)
    # len(uid_present_one_time_s)-1 -> removing the header
    return [len(uid_present_one_time_s)-1, len(uid_present_two_time_or_more_s)]


# Exercice 2
def get_uid_set_by_group(csv_file_path):
    """
    This function return a dict of set of each uid present in every group like
    {
        group_id_1:{uid_1,uid_2,...},
        group_id_2:{uid_1,uid_5,...},
        ...
    }
    :param csv_file_path: csv file path of file formatted like 'uid;group'
    :type csv_file_path: String
    :return: dict of set.
    """
    uid_set_by_group_d = dict()
    with open(csv_file_path, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t')
        for uid, group in spamreader:
            if group in uid_set_by_group_d.keys():
                if uid not in uid_set_by_group_d[group]:
                    uid_set_by_group_d[group].add(uid)
            else:
                uid_set_by_group_d[group] = {uid}
    # remove the header key from dict
    uid_set_by_group_d.pop('group', None)
    return uid_set_by_group_d


def number_user_in_common_df(csv_file_path):
    """
    Create a matrix of number of uid in common between each group from a csv file
    :param csv_file_path: csv file path of file formatted like 'uid;group'
    :type csv_file_path: String
    :return: panda DataFrame of number of uid in common between each group
    """
    uid_set_by_group_d = get_uid_set_by_group(csv_file_path)
    df_dict = dict()
    for group_a in uid_set_by_group_d.keys():
        if group_a not in df_dict.keys():
            df_dict[group_a] = dict()
        for group_b in uid_set_by_group_d.keys():
            if group_a == group_b:
                df_dict[group_a][group_b] = len(uid_set_by_group_d[group_a])
            else:
                if group_b in df_dict.keys() and group_a in df_dict[group_b].keys():
                    df_dict[group_a][group_b] = df_dict[group_b][group_a]
                else:
                    if group_a not in df_dict:
                        df_dict[group_a] = dict()
                    df_dict[group_a][group_b] = len(uid_set_by_group_d[group_a].intersection(uid_set_by_group_d[group_b]))
    return pandas.DataFrame(df_dict)


# Exercice 3
def aggregate_visit_files_to_df(file_path_list):
    """
    Aggregate list of nb visit per uid file
    :param file_path_list: list of csv files formatted like 'uid;nb_visit'
    :type file_path_list: list of String
    :return: panda DataFrame of number of visit for each id from two file
    """
    df_list = []
    for csv_file in file_path_list:
        df_list.append(pandas.read_csv(csv_file, delimiter=';', header=None, names=['uid', 'nb_visit']))
    result_df = pandas.concat(df_list).groupby('uid').sum()
    return result_df


if __name__ == "__main__":

    EXAMPLE_FILE_PATH_PART_A = "data/users_groups.csv"
    EXAMPLE_FILE_PATH_LIST_PART_B = ["data/nb_visites1.csv", "data/nb_visites2.csv"]
    EXEC_NUMBER = 10

    exercice_1_object = count_uid_appear_one_or_more_in_csv(EXAMPLE_FILE_PATH_PART_A)
    exercice_1_timeit_wrapped = timeit_wrapper(count_uid_appear_one_or_more_in_csv, EXAMPLE_FILE_PATH_PART_A)
    average_time_exec_ex_1 = timeit.timeit(exercice_1_timeit_wrapped, number=EXEC_NUMBER)
    exercice_2_object = number_user_in_common_df(EXAMPLE_FILE_PATH_PART_A)
    exercice_2_timeit_wrapped = timeit_wrapper(number_user_in_common_df, EXAMPLE_FILE_PATH_PART_A)
    average_time_exec_ex_2 = timeit.timeit(exercice_2_timeit_wrapped, number=EXEC_NUMBER)
    exercice_3_object = aggregate_visit_files_to_df(EXAMPLE_FILE_PATH_LIST_PART_B)
    exercice_3_timeit_wrapped = timeit_wrapper(number_user_in_common_df, EXAMPLE_FILE_PATH_PART_A)
    average_time_exec_ex_3 = timeit.timeit(exercice_3_timeit_wrapped, number=EXEC_NUMBER)

    print("Alexandre ENOUF : test python 2018")
    print("Librairies/packages utilisés : pandas, csv & timeit")
    print("----------")
    print("Exercice 1")
    print("Fichier %s : uid present une seule fois %s, uid present plusieurs fois %s" % (EXAMPLE_FILE_PATH_PART_A,
                                                                                         exercice_1_object[0],
                                                                                         exercice_1_object[1]))
    print("Temps moyen d'execution sur %s executions : %s s" % (EXEC_NUMBER, str(average_time_exec_ex_1 / EXEC_NUMBER)))
    print("----------")
    print("Exercice 2")
    print("L'object construit est un DataFrame panda :")
    print(exercice_2_object)
    print("Temps moyen d'execution sur %s executions : %s s" % (EXEC_NUMBER, str(average_time_exec_ex_2 / EXEC_NUMBER)))
    print("----------")
    print("Exercice 3")
    print("L'object construit est un DataFrame panda :")
    print(exercice_3_object)
    print("Temps moyen d'execution sur %s executions : %s s" % (EXEC_NUMBER, str(average_time_exec_ex_3 / EXEC_NUMBER)))
