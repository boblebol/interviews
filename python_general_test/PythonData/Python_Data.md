#Test recrutement

Pour les deux exercices, merci de joindre aux résultats le code source (script ou notebook python)
J'ai utilisé les librairies/package pandas, matplotlib et seaborn.

### Premier exercice
Les données en PJ (_data/Summer_Olympic_medallists_1896_to_2008.csv_) sont la liste des athlètes ayant
gagné une médaille aux jeux olympiques entre 1896 et 2008.

* Quels sont les 3 pays ayant gagnés le plus de médailles ?
    * Les trois pays ayant gagné le plus de médailles aux jeux olympique sont :  les USA, l'URSS et la Grande Bretagne
* Donner le nombre de médailles de chaque type (or, argent et bronze) des 10 pays ayant gagnés le plus de médailles sur l’ensemble de la période
    * Nombre de médailles de chaque type des 10 pays ayant gagnés le plus de médailles 

| PAYS | OR   | ARGENT | BRONZE|
|:----:|:----:|:------:|:-----:|
| USA  | 2087 | 1195   | 1052  |
| URS  | 838  | 627    | 584   |
| GBR  | 498  | 591    | 505   |
| FRA  | 378  | 461    | 475   |
| ITA  | 460  | 394    | 374   |
| GER  | 407  | 350    | 454   |
| AUS  | 293  | 369    | 413   |
| HUN  | 400  | 308    | 345   |
| SWE  | 347  | 349    | 325   |
| GDR  | 329  | 271    | 225   |

* Quel pays a réalisé la meilleure progression sur les 4 dernières éditions des JO
    * Sur les quatres dernières éditions des JO la Chine a réalisée la meilleure progression en terme de gain de médailles (tous types confondu) avec un différentiel de 106 médailles entre 1996 et 2008.
* Est-ce que le fichier comporte des données manquantes ou incohérentes ?
    * Pas de réponse
* Faire un graphe de l’évolution du nombre de médailles gagnés pour la Chine et les Etats-Unis. Que remarquez-vous ?
    * Executer le fichier _python_data.py_
* Faire un graphe représentant la repartions homme/femme des gagnants aux JO.
    * Executer le fichier _python_data.py_
* Avez-vous d’autres remarques sur le jeu de données ?
    * Pas de remarques


### Second exercice
Crimes à Chicago

Les données en PJ (_data/Crimes_chicago_2018.csv_) sont l’ensemble des incidents ou crimes (hors
assassinat) qui a été répertorié par la ville de Chicago entre 2001 et 2018.

Analysez le fichier afin d’extraire des tendances ou des phénomènes à partir des données.

#### Solution

Tous les graphiques sont accessibles en lancant le code du fichier _python_data.py_
 
Pour les crimes de Chicago, j'ai commencé par faire une heatmap des corrélations du dataframe (fonction proposée par la bibliotheque pandas).

Ne trouvant pas de corrélation flagrante, j'ai fais des graphique de fréquences par type de données. Grâce à ces graphique nous pouvons voir la fréquence par type, par quartiers, par mois , si arrestation, si crime domestiques et par code du FBI.

Nous pouvons relever que le nombre de crime a tendance à augmenter au cours de l'année, mais aussi que les vols sont le crime le plus commun .

On peut constater alors qu'il y a des graphiques qui se ressemblent comme celui de la fréquence des code fbi ainsi que la fréquece des différent types de crimes. 
J'ai donc fais des graphiques croisés entre deux types de données telles que les types de crime ainsi que le code fbi, ainsi nous pouvons voir à quel type de crime corresponds chaque code. J'ai aussi croisé le type de crime et les districts, nous pouvons voir que certains districts paraissent epargnés par certains types de crimes.

Je n'ai pas poursuivi l'étude des données plus que cela, j'ai juste relevé quelques tendances et corrélations.
