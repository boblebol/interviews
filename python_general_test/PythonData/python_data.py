#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Alexandre ENOUF : test python 2018
# Librairies/packages utilisés : pandas, csv & timeit

import datetime
import re

import matplotlib.pyplot as plt
import pandas
import seaborn

SUMMER_OLYMPIC_MEDALLISTS_FILE_PATH = "data/Summer_Olympic_medallists_1896_to_2008.csv"
SUMMER_OLYMPIC_MEDALLISTS_HEADER_LIST = ['City', 'Edition', 'Sport', 'Discipline', 'Athlete',
                                         'NOC', 'Gender', 'Event', 'Event_gender', 'Medal']
CRIME_CHICAGO_FILE_PATH = "data/Crimes_chicago_2018.csv"
CRIME_CHICAGO_HEADER_LIST = ['ID', 'Case Number', 'Date', 'Block', 'IUCR', 'Primary Type', 'Description',
                             'Location Description', 'Arrest', 'Domestic', 'Beat', 'District', 'Ward', 'Community Area',
                             'FBI Code', 'X Coordinate', 'Y Coordinate', 'Year', 'Updated On', 'Latitude', 'Longitude',
                             'Location']


def csv_to_df(filepath, column_name_list):
    """
    transform a csv file to DF with dataformats
    :param filepath: File path of the csv file
    :param column_name_list: column name list
    :return: pandas data frame
    """
    medalist_list_information_l = []
    with open(filepath, 'r') as csvfile:
        for line in csvfile:
            formatted_line_1 = re.sub(r'"\((?P<prem>[^,]+), (?P<suiv>[^,]+)\)"', r'(\g<prem>; \g<suiv>)',
                                      line.replace('\n', ''))
            formatted_line = re.sub(r',""(?P<prem>[^,]+)(, (?P<suiv>[^,]+))+"",', r',\g<prem> \g<suiv>,',
                                    formatted_line_1)
            medalist_information_l = formatted_line.replace('"', '').split(',')
            if column_name_list != medalist_information_l:
                if len(medalist_information_l) == len(column_name_list):
                    medalist_list_information_l.append(medalist_information_l)

    return pandas.DataFrame(medalist_list_information_l, columns=column_name_list)


def question_1(df, number):
    noc_df = df.groupby("NOC").count()
    return noc_df.sort_values(by=["Medal"], ascending=False).index.tolist()[:number]


def question_2(df, number):
    top_noc_list = question_1(df, number)
    result_dict = dict()
    for noc in top_noc_list:
        number_of_gold = df.loc[(df['NOC'] == noc) & (df["Medal"] == "Gold")].count()["Medal"]
        number_of_silver = df.loc[(df['NOC'] == noc) & (df["Medal"] == "Silver")].count()["Medal"]
        number_of_bronze = df.loc[(df['NOC'] == noc) & (df["Medal"] == "Bronze")].count()["Medal"]
        result_dict[noc] = {'Gold_Medal': number_of_gold, 'Silver_Medal': number_of_silver, 'Bronze_Medal': number_of_bronze}
    return pandas.DataFrame(result_dict)


def question3(df, number):
    edition_list = df["Edition"].unique().tolist()[number*(-1):]
    noc_list = df["NOC"].unique().tolist()
    result_dict = dict()
    max_progression = 0
    for noc in noc_list:
        medal_before = df.loc[(df['NOC'] == noc) & (df['Edition'] == edition_list[0])].count()["Medal"]
        medal_now = df.loc[(df['NOC'] == noc) & (df['Edition'] == edition_list[-1])].count()["Medal"]
        progression = medal_now-medal_before
        if progression > max_progression:
            result_dict = {noc: progression}
            max_progression = progression
    return result_dict


def question5(df):
    edition_list = df["Edition"].unique().tolist()
    difference_medal_dict = dict()
    for edition in edition_list:
        usa_nb_medal = df.loc[(df['NOC'] == "USA") & (df['Edition'] == edition)].count()["Medal"]
        china_nb_medal = df.loc[(df['NOC'] == "CHN") & (df['Edition'] == edition)].count()["Medal"]
        if usa_nb_medal != 0 and china_nb_medal != 0:
            difference_medal_dict[edition] = max(usa_nb_medal,china_nb_medal)-min(usa_nb_medal,china_nb_medal)
    x_diff, y_diff = zip(*sorted(difference_medal_dict.items()))
    plt.plot(x_diff, y_diff, color='black')
    plt.title("Difference of medals won between China and the USA since 1984.")
    plt.xlabel("Year")
    plt.ylabel("Difference of medal won")
    plt.savefig("images/question5.png")
    plt.close()


def question6(df):
    edition_l = df["Edition"].unique().tolist()
    men_nb_medal_d = dict()
    women_nb_medal_d = dict()
    for edition in edition_l:
        men_nb_medal=df.loc[(df['Event_gender'] == "M") & (df['Edition'] == edition)].count()["Medal"]
        women_nb_medal=df.loc[(df['Event_gender'] == "W") & (df['Edition'] == edition)].count()["Medal"]
        men_nb_medal_d[edition] = men_nb_medal
        women_nb_medal_d[edition] = women_nb_medal
    x_men, y_men = zip(*sorted(men_nb_medal_d.items()))
    x_wo, y_wo = zip(*sorted(women_nb_medal_d.items()))

    plt.bar(x_men, y_men, color='blue', label='nb of medal won by men')
    plt.bar(x_wo, y_wo, color='pink', label='nb of medal won by women')
    plt.title("Difference of medals won between men and women.")
    plt.xlabel("Year")
    plt.ylabel("Nb medal won")
    plt.legend(loc='lower right', shadow=False, fontsize='x-small')
    plt.savefig("images/question6.png")
    plt.close()


def create_bar_frequence_type(df,column,save_file_path):
    """
    Create a bar diagram of frequences of different value for a specific column
    :param df: panda dataframe
    :param column: The column you want to see the frequency
    :param save_file_path: The file path where you want to save the graph
    :return:
    """
    fig, ax = plt.subplots()
    df[column].value_counts().plot(ax=ax, kind='bar')
    fig.savefig(save_file_path)
    plt.close()


def create_heatmap(df, save_file_path):
    """
    Create a heatmap of correlation in dataframe
    :param df: panda dataframe
    :param save_file_path: The file path where you want to save the graph
    """
    fig, ax = plt.subplots(figsize=(10, 8))
    correlations = df.corr()
    seaborn.heatmap(correlations, cmap=seaborn.diverging_palette(220, 10, as_cmap=True),square=True, ax=ax)
    fig.savefig(save_file_path)


def create_scratterplot(df,column_1, column_2, save_file_path):
    """
    create a graph to see correlation betwen values in two columns of a df

    :param df: panda dataframe
    :param column_1: column with value on the x-axis
    :param column_2: column with value on the y-axis
    :param save_file_path: The file path where you want to save the graph
    :return:
    """
    fig, ax = plt.subplots(figsize=(16, 10))
    seaborn.scatterplot(x=column_1, y=column_2, data=df, ax=ax)
    fig.savefig(save_file_path)


if __name__ == "__main__":
    seaborn.set()
    print ("Exercice 1")
    medal_df = csv_to_df(SUMMER_OLYMPIC_MEDALLISTS_FILE_PATH, SUMMER_OLYMPIC_MEDALLISTS_HEADER_LIST)
    print("question 1")
    print(question_1(medal_df, 3))
    print("question 2")
    print(question_2(medal_df, 10))
    print("question 3")
    print(question3(medal_df, 3))
    question5(medal_df)
    question6(medal_df)
    print("Les graphiques des questions 5 & 6 sont dans le dossier images/")
    print("----------")
    print("Exercice 2")
    chicago = pandas.read_csv(CRIME_CHICAGO_FILE_PATH)
    print("Création d'une heatmap des corrélations entre les différentes colonnes du fichier")
    create_heatmap(chicago, "heatmap.png")
    print("Création de graphiques de frequences de certaines colonnes")
    create_bar_frequence_type(chicago, "Arrest", "images/arrest_frequency.png")
    create_bar_frequence_type(chicago, "Domestic", "images/domestic_frequency.png")
    create_bar_frequence_type(chicago, "District", "images/district_frequency.png")
    create_bar_frequence_type(chicago, "Primary Type", "images/primary_type_frequency.png")
    create_bar_frequence_type(chicago, "FBI Code", "images/fbi_code_frequency.png")
    print("Création de graphiques en fonction de deux colonnes")
    create_scratterplot(chicago, 'District', "Primary Type", "images/district_primary_type.png")
    create_scratterplot(chicago, "FBI Code", 'Primary Type', "images/fbi_code_primary_type.png")
    chicago["Date"] = chicago["Date"].apply(lambda x: datetime.datetime.strptime(x, "%m/%d/%Y %H:%M:%S %p").month)
    create_scratterplot(chicago, 'Date', "Arrest", "images/month_arrest.png")
    create_bar_frequence_type(chicago, "Date", "images/month_frequency.png")
    create_scratterplot(chicago, 'Date', "Primary Type", "images/month_Primary_type.png")
