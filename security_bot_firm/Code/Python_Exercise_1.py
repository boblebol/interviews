# -*- coding: utf-8 -*-

import string


def is_palindrome(test_str):
    format_str = ''.join(test_str.split()).lower()
    format_str = ''.join(c for c in format_str if c not in string.punctuation)
    len_str = int(len(format_str))
    if len_str == 0 or len_str == 1:
        return True
    i = 0
    while i < len_str//2:
        if format_str[i] != format_str[len_str-i-1]:
            return False
        i += 1
    return True


print("'' is a palindrome : "+str(is_palindrome("")))
print("'o' is a palindrome : "+str(is_palindrome("o")))
print("'bb' is a palindrome : "+str(is_palindrome("bb")))
print("'bob' is a palindrome : "+str(is_palindrome("bob")))
print("' A man, a plan, a canal, Panama. ' is a palindrome : "+str(is_palindrome(" A man, a plan, a canal, Panama. ")))

print("'ab' is a palindrome : "+str(is_palindrome("ab")))
print("'boba' is a palindrome : "+str(is_palindrome("boba")))
print("'Alexandre' is a palindrome : "+str(is_palindrome("Alexandre")))
