# -*- coding: utf-8 -*-

import string


def is_anagram(first_str, second_str):
    format_first_str = ''.join(first_str.split()).lower()
    format_first_str = ''.join(c for c in format_first_str if c not in string.punctuation)
    format_second_str = ''.join(second_str.split()).lower()
    format_second_str = ''.join(c for c in format_second_str if c not in string.punctuation)
    return ''.join(sorted(format_first_str)) == ''.join(sorted(format_second_str))


print("'astronomers' is an anagram of 'no more stars' : "+str(is_anagram("astronomers", "no more stars")))
print("'debit card' is an anagram of 'bad credit' : "+str(is_anagram("debit card", "bad credit")))
print("'funeral' is an anagram of 'real fun' : "+str(is_anagram("certainly not", "can't rely on it")))

print("'basket-ball' is an anagram of 'football' : "+str(is_anagram("basket-ball", "football")))
print("'test' is an anagram of '' : "+str(is_anagram("test", "")))
