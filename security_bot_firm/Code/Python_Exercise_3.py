# -*- coding: utf-8 -*-


def ip_str_to_ip_bin(ip_str):
    """
    Translate an ip string formated like 'xxx.x.xx.xxx' in string of binaire of this ip
    :param ip_str:
    :return: ip formated like '10100110.00000000.00000000.11111111'
    """
    ip_str_without_cidr = ip_str.split('/')[0]
    return '.'.join([bin(int(x)+256)[3:] for x in ip_str_without_cidr .split('.')])


def common_start(sa, sb):
    """
    returns the longest common substring from the beginning of sa and sb
    :param sa:
    :param sb:
    :return: longest common substring from the beginning of sa and sb
    """
    def _iter():
        for a, b in zip(sa, sb):
            if a == b:
                yield a
            else:
                return
    return ''.join(_iter())


def common_subnet(ipa, ipb):
    """
    return the common ip subnet in CIDR notation between two ip or ip subnet in CIDR notation
    :param ipa: first ip or ip subnet in CIDR notation
    :param ipb: second ip or ip subnet in CIDR notation
    :return: common ip subnet in CIDR or None
    """
    if ipa == ipb:
        return ipa
    ipa_split = ipb.split('.')
    ipb_split = ipa.split('.')
    if ipa_split[0] != ipb_split[0]:
        return None
    ip_subnet_result = ''
    index_ip_split = 0
    while index_ip_split < len (ipb_split) and ipa_split[index_ip_split] == ipb_split[index_ip_split]:
        ip_subnet_result += ipa_split[index_ip_split]+'.'
        index_ip_split += 1
    cidr_subnet_part = index_ip_split * 8
    ipa_bin_split = ip_str_to_ip_bin(ipa).split('.')
    ipb_bin_split = ip_str_to_ip_bin(ipb).split('.')
    cidr_subnet_part += len(common_start(ipa_bin_split[index_ip_split], ipb_bin_split[index_ip_split]))
    while index_ip_split <= len(ipb_split)-1:
        if index_ip_split == 3:
            ip_subnet_result += '0/'+str(cidr_subnet_part)
        else:
            ip_subnet_result += '0.'
        index_ip_split += 1
    return ip_subnet_result


def sort_ip_list(ip_list):
    """
    Given a list of IPs and IP subnets in valid CIDR notation,
    return a new list of IPs and valid CIDR subnets that represents the same list IPs but using the least amounts
    of IPs and subnets possible.
    My solution is false because I use the simple sorting (alphabetic index).
    To make it work you have to use a correct ip sort, if you do that the rest work perfectly.
    :param ip_list:
    :return:list of IPs and valid CIDR subnets that represents the same list IPs but using the least amounts
    of IPs and subnets possible.
    """
    if len(ip_list) == 0 or len(ip_list) == 1:
        return ip_list
    sorted_ip_list = sorted(ip_list) # You have to change this sort to sort by ip not alphabetic
    result_ip_list = [sorted_ip_list[0]]
    for ip in sorted_ip_list:
        common_cidr_subnet = common_subnet(ip, result_ip_list[-1])
        if common_cidr_subnet:
            result_ip_list[-1] = common_cidr_subnet
        else:
            result_ip_list.append(ip)
    return result_ip_list


print("Sort ['192.168.0.0/23', '192.168.2.0/23', '192.168.0.13', '168.0.0.1'] : " +
      str(sort_ip_list(['192.168.0.0/23', '192.168.2.0/23', '192.168.0.13', '168.0.0.1'])))
print("Sort ['192.168.0.0', '192.168.1.0', '192.168.2.0', '192.168.3.0'] : " +
      str(sort_ip_list(['192.168.0.0', '192.168.1.0', '192.168.2.0', '192.168.3.0'])))
print("Sort ['192.168.0.0', '192.168.1.0', '192.168.2.0', '192.168.3.0', '192.168.0.0', '192.168.1.0', '192.168.2.0', '192.168.3.0'] : " +
      str(sort_ip_list(['192.168.0.0', '192.168.1.0', '192.168.2.0', '192.168.3.0',
                        '192.168.0.0', '192.168.1.0', '192.168.2.0', '192.168.3.0'])))
print("Sort ['192.168.0.0', '193.168.1.0', '194.168.2.0', '195.168.3.0'] : " +
      str(sort_ip_list(['192.168.0.0', '193.168.1.0', '194.168.2.0', '195.168.3.0'])))
print("Sort ['192.161.0.0', '192.162.1.0', '192.169.2.0', '192.167.3.0'] : " +
      str(sort_ip_list(['192.161.0.0', '192.162.1.0', '192.169.2.0', '192.167.3.0'])))
