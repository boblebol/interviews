#! /bin/bash
# copy_script.sh

src_dir="$1"
dest_dir="$2"

find $src_dir -name '*.txt' | while read file;do
	src_file=$file
	intermediate_dest_file=${file/$src_dir/$dest_dir}
	old_filename=`basename $file`
	new_filename="new_$old_filename"
	dest_file=${intermediate_dest_file/$old_filename/$new_filename}
	mkdir -p `dirname $dest_file`
	cp $src_file $dest_file
done