# Exercise made for a firm specialised in search data leaks

## Summary

Three exercise on python and/or scripts

* Shell script exercise
* Python exercises
* Open questions

## Shell script exercise



### Statement
Write a command line or a script (bash, fish, zsh or Python3 allowed) that copies every .txt files from a specified directory to another one, adding «new» in front of each filename.

### Solution 
**Explaination**

For this exercise I chose to use a bash script, because file management is very easy in bash and a single command line can be hard to read and/or understand  by someone-else. 
With the loop on the result of command *find* and a simple substitution I can do this without recursion if there is a folder tree in the folder of the first argument.

**Code :**

*./Code/copy_script.sh*


## Python script exercise

### Statement 1
A palindrome is a string that is the same forwards and backwards, disregarding non-letters and case. Eg. " A man, a plan, a canal, Panama. " is a palindrome. Write a function that takes a string as input, and returns wether it's a palindrome.

### Solution
**Explanation :**

* First step is cleaning the String. 
* If the String have zero or one letter the word is a palindrome.
* Second step we have to compare the first and last letter, then the second and the last but one letter, and so on, until we arrive at the middle of the word.
If everything goes well it's a palindrome, if at any moment the two letters compared are different the word it's not.

**Code :**

*Python_Exercise_1.py*

### Statement 2
 Write a function that takes two strings as arguments, and returns true if they are anagrams of each other, and false otherwise.
### Solution
**Explanation :**

**Code :**

*Python_Exercise_2.py*

**Another possible solution :**

We can use a counter object. (cf: https://docs.python.org/2/library/collections.html#collections.Counter)

### Statement 3
Given a list of IPs and IP subnets in valid CIDR notation, return a new list of IPs and valid CIDR subnets that represents the same list IPs but using the least amounts of IPs and subnets possible.
### Solution
**Explanation :**

For this exercise the algorithm I use is the following one:
After sorting the argument list I create a result list and add ip or subnet from argument list if I can't find a valid cidr subnet with an element of the result list, If I can find a valid cidr subnet between an element from both lists, I replace the element in the result list by the valid CIDR subnet I found.
To apply this algorithm I develop three more functions :
One to find a valid CIDR subnet between two element, this function use the function to find the common start between two string and the function to translate ip in base 10 to ip in base 2.

**EDIT :** **_My solution is false because I use the simple alphabetic sorting instead of ip sorting in the last function._**

This remark is reported in the file at the correct place

**Code :**

*Python_Exercise_3.py*


## Open questions

### statement
Given the following code snippet:
```python
URL = "https://users.example.com/api/"
class Authenticator:
   def authenticate(user, pwd)
     xml = "<login><username>%s</username><password>%s</password></login>"%(user, pwd)
     res = post_request(URL, xml) # post_request is defned elsewhere
     if res.code == 200:
         return true
     else:
         return false
```
What’s your general impression of the code at first sight? Can you point out any bug or potential bug in it? How would you improve it?

### Solution
This code snippet seems to be a non tested draft function for several reasons.

* Here I correct syntax error and add some comments to make a better code:
    * Here is some syntax error so the code can't run :
        * Missing ':' for define a function :
        ```python
        def authenticate(user, pwd):
        ```
        * Boolean are write with first capital letters in python :
        ```python
        if res.code == 200:
            return True
        else:
            return False
        ```
    * Something for specify the definition of the method authenticate in the object, three possibilities :
        * make this a class method, which execution refer always to the class.
        ```python
        @classmethod
        def authenticate(cls, user, pwd):
        ```
        * make this an instance method, which execution refer always to a specific object.\newline
        ```python
        def authenticate(self, user, pwd):
        ```
        * make this a static method, which are used to group functions which have some logical connection with a class to the class.\newline
        ```python
        @staticmethod
        def authenticate(user, pwd):
        ```
        As we see, without *cls* or *self* in function parameters I think the developer want to make it a static method.   
    * Some comments to make the code more understandable so more maintainable :
        * Adding doc string\newline
        * Adding some logs (an object logger from *logging* library for example)
        * Making some unit tests (with the package *pytest* and a mock for simulate the response from the API for example)
        * Making some user acceptance tests to be sure the connection method make what we want (we can use the BDD tool *radish* to describe a scenario to be sure the program have the comportment and the result we want)
        * Making the URL variable an environment variable and get it from a definition file, it's easier if we have to use this url in other python file and we can change this variable without modify the code
        
        ```python
        # -*- coding: utf-8 -*-
        # define.py
        
        URL = os.environ.get('URL', 'https://users.example.com/api/')
        ```
        ```python
        # -*- coding: utf-8 -*-
        # code_snippet.py
        
        from define import URL
    
        class Authenticator:
            ...
            ...
            res = post_request(URL, xml)
            ...
        ```
    * Some comments about the API called and the snippet:
        * May be the API must return an authentication token that will be used during the next call to identify which user comes calling and if he has the necessary rights for these calls.
        * In this case, the class *Authenticator* from the snippet must get the token in an instance variable, and the method authenticate is an instance method and mush have *self* in first parameter.
        

### statement
What do you think are the main challenges of our infrastructure? What could be some ideas to overcome them?
### Solution
Without knowing the software architecture of CybelAngel, I can just give some "classic" recommendations.
* Architecture of the code
    * The code will change so the code must be very organized to simplify evolution or change. I propose that the code follow a micro-services architecture.
        * All brick must be independent to commutable for future developments.
        * All brick must be tested and have CI and CD
    * We can make evolve and deploy quickly the product, the tests limiting the risk of regression and ensuring a code that works.

* Scalability
    * To manage a large number of servers here is some tool to do this efficiently like Chef or Puppet or Ansible.
    * Dockerize our programs allows us to separate all our programs in different boxes to avoid compatibility or version problems. Dockerizing our infrastructure helps to develop in a simulated target environment.\newline
    * Dockerization is one of the easy scalability solution with some tools like Kubenetes to easily administer and create new Docker containers .\newline 
    * To distribute the work among all the Dockers, I think that putting a message broker like Kafka or RabbitMQ is a solution, each docker takes the message and processes it without having to solve synchronization problems.
