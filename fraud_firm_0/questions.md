
### Variable independency
> What does X and Y independent implies?

* P(X & Y) = P(X) * P(Y)
* P(X | Y) = P(X)

### Correlation
> Corr(X, Y) > 0, can X and Y be independent?

* No, there is a linear dependency.

> Corr(X, Y) = 0, can X and Y be dependent?

* Yes, example: X = Y²

### Sets
> How many sets can be make with 3 elements?

* Each element can be present once or absent => 2 states => 2^3

### Coin tosses
> 100 coins, one has two head side.
>
> P(coin is fake | 10 head tosses) = ?

* P(A|B) = P(A & B) / P(B)
* P(A) = P(A|B) * P(B) + P(A|/B) * P(/B)


* P(10 head tosses | coin is legit) = (1 / 2)^10
* P(10 head tosses | coin is fake) = 1
* P(coin is legit) = 99/100
* P(coin is fake) = 1 / 100


* P(10 head tosses) = P(10 head tosses | coin is legit) \* P(coin is legit) + P(10 head tosses | coin is fake) \* P(coin is fake)
* P(coin is fake | 10 head tosses) = P(coin is fake & 10 head tosses) / P(10 head tosses)
* P(coin is fake | 10 head tosses) = P(E)


* P(E) = P(coin is fake) / (P(10 head tosses | coin is legit) \* P(coin is legit) + P(10 head tosses | coin is fake) \* P(coin is fake))
* P(E) = (1/100) / ( (1 / 2)^10 \* 99/100) + 1 \* 1/100 )
* P(E) = 1 / (1 + 99/2^10)
* P(E) = 1 / (1 + 99/1024)
* P(coin is fake | 10 head tosses) = 91%

### Modified Russian roulette
> 1 gun, 6 holes, 2 consecutive bullets.
>
> Gun is rolled once, a player triggers the gun and nothing happens.
>
> It is your turn, do you roll the gun and trigger it or trigger it directly?

* Draw a gun with 6 holes and 2 bullets.
* The gun is in one of 4 positions: 1 bullet or 3 holes.
* P(trigger and survive) = 3 / 4 = 75%
* P(roll and trigger and survive) = 2 / 6 = 2 / 3 = 66.6%

### Family poll
> A poll has been made to families with the following questions:
>
> P: How many children do you have? (asking to both parents)
>
> C: How many children are you in your family? (asking the children)
>
> Why does the average of the 2nd question higher than the 1st?

Mean(P) < Mean(C) because of:
* N: number of children
* P is O(2*N)
* C is O(N²)

### Infinite corridor
> You have a corridor in a donut shape, potentially infinite, split by doors.
>
> A door cannot stay open. (doors are not usable for this problem)
>
> In each room, you have a light switch and a light. Initially it is randomly turned on or off.
>
> You start in room 0. How can you know if you went back to your initial position?

I leave the initial room light turned on.

I go through one of the two doors, and turn off each light as I go from rooms
to rooms and count each room passed.

Whenever I need to know whether I went back to the initial room, I just have to
walk back the amount of rooms I went through.

If room 0 light is still turned on then I have been back there otherwise I have
been walking in circle.

I may turn it on again to have a referential in the donut corridor.

### Maximizing the consecutive sub-list sum within a list
> You have a list of signed integer.
> How do you find the sub-list with the highest sum?
> (the sub-list is continued)

* list = [e(0), ..., e(i), ..., e(j), ..., e(n-1)]
* SUM(sub-list) = SUM(i, j) = SUM(0, j) - SUM(0, i-1)
* We maximise SUM(0, j) and minimize SUM(0, i-1).

### Cleaning phone numbers
> You have a list of {user_id, phone_numbers}, how do you find those that are not real?

* flag which phone number appears not to be phone number
* check phone number count distribution
  * a phone number attached to too many (i.e. far from average) user_id is likely to be a bad one, flagging these
  * checker whether some phone count above average have a given pattern looking
  like a fake one (ex: 0123456789)
  * defining bad pattern and checking these phone numbers
