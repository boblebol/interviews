# coding: utf-8

import os
import pandas as pd

from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import classification_report
from sklearn.multiclass import OneVsRestClassifier


def load_dataset(filename):
    dataset = []

    with open(filename, 'r') as f:
        for line in f.read().splitlines():
            intent, expression = line.split(' ', 1)
            dataset.append((intent, expression))

    return pd.DataFrame(data=dataset, columns=['intent','sentence'])

def prepare_datasets(train, test, label_col = 'intent', feature_col = 'sentence'):
    idf = TfidfVectorizer()
    train_x = idf.fit_transform(train[feature_col])
    test_x = idf.transform(test[feature_col])
    return train_x, train[label_col], test_x, test[label_col]

def train_and_eval(train_x, train_y, test_x, test_y):
    model = RandomForestClassifier(n_estimators=200, random_state=1)
    model = OneVsRestClassifier(model)
    model.fit(X=train_x, y=train_y)
    print(classification_report(y_pred=model.predict(test_x), y_true=test_y))


if __name__ == '__main__':
    print('Train')
    train = load_dataset(os.path.join('data', 'train.txt'))
    print(train.sample(n=5, random_state=1))

    print('\nTest')
    test = load_dataset(os.path.join('data', 'test.txt'))
    print(test.sample(n=5, random_state=1))

    print('\npreparing data (tf-idf)')
    train_x, train_y, test_x, test_y = prepare_datasets(train, test)

    print('\ntraining and evaluating model')
    train_and_eval(train_x, train_y, test_x, test_y)

    # possible improvements:
    # - use a pipeline
    # - use grid search to determine tf-idf and RF best parameters
    # - investigate classes with lower scores

    #                 precision    recall  f1-score   support
    #
    #        arrival       0.88      0.81      0.84       247
    #      departure       0.83      0.74      0.78       210
    #      greetings       0.64      0.97      0.77        60
    #         return       0.93      0.93      0.93        46
    #       schedule       0.91      0.86      0.88       262
    #        support       0.67      1.00      0.80         6
    #           trip       0.85      0.89      0.87       592
    #troubleshooting       1.00      0.89      0.94        35
    #
    #    avg / total       0.86      0.86      0.86      1458
