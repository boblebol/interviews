import unittest

from src.node import Node


class TestNodeInit(unittest.TestCase):

    def test_construction(self):
        n1 = Node(value=1)
        n2 = Node(value=2, left=n1, right=n1)
        self.assertEqual(n2.value, 2)
        self.assertEqual(n2.left, n1)
        self.assertEqual(n2.right, n1)

    def test_value_should_not_be_float(self):
        try:
            n3 = Node(value=1.5)
            raise ValueError('should have failed with {}'.format(n3))
        except ValueError:
            # failed correctly
            pass

    def test_value_should_not_be_string(self):
        try:
            n4 = Node(value='1')
            raise ValueError('should have failed with {}'.format(n4))
        except ValueError:
            # failed correctly
            pass

    def test_tree_should_be_balanced(self):
        try:
            n5 = Node(value='1', left=Node(value=1))
            raise ValueError('should have failed with {}'.format(n5))
        except ValueError:
            # failed correctly
            pass
