import unittest

from src.node import Node


class TestNodeMax(unittest.TestCase):

    def test_max_value_1(self):
        # leafs
        l11 = Node(value=1)
        l12 = Node(value=2)
        l13 = Node(value=5)
        l14 = Node(value=3)

        # mid level
        l21 = Node(value=4, left=l11, right=l12)
        l22 = Node(value=0, left=l13, right=l14)

        # root = tree
        root = Node(value=2, left=l21, right=l22)

        self.assertEqual(root.get_max_value(), 5)

    def test_max_value_2(self):
        # leafs
        l11 = Node(value=1)
        l12 = Node(value=2)
        l13 = Node(value=3)
        l14 = Node(value=3)

        # mid level
        l21 = Node(value=4, left=l11, right=l12)
        l22 = Node(value=0, left=l13, right=l14)

        # root = tree
        root = Node(value=2, left=l21, right=l22)

        self.assertEqual(root.get_max_value(), 4)

    def test_max_value_3(self):
        # leafs
        l11 = Node(value=1)
        l12 = Node(value=2)
        l13 = Node(value=5)
        l14 = Node(value=3)

        # mid level
        l21 = Node(value=4, left=l11, right=l12)
        l22 = Node(value=0, left=l13, right=l14)

        # root = tree
        root = Node(value=20, left=l21, right=l22)

        self.assertEqual(root.get_max_value(), 20)
